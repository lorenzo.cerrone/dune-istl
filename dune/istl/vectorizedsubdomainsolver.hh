// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_ISTL_VECTORIZEDSUBDOMAINSOLVER_HH
#define DUNE_ISTL_VECTORIZEDSUBDOMAINSOLVER_HH

namespace Dune{

  template<class S, class X, class Y>
  class VectorizedSubdomainSolver :
    public InverseOperator<X,Y>
  {
    S solver_;
    typedef typename S::domain_type solver_domain;
    typedef typename S::range_type solver_range;
  public:
    typedef X domain_type;
    typedef typename X::field_type field_type;
    typedef Y range_type;
    typedef typename S::matrix_type matrix_type;
    typedef typename S::MatrixInitializer MatrixInitializer;

    template<class... Args>
    VectorizedSubdomainSolver(Args&&... args)
      : solver_(std::forward<Args>(args)...)
    {}

    virtual SolverCategory::Category category() const{
      return solver_.category();
    }

    void apply(domain_type& x, range_type& y){
      InverseOperatorResult res;
      this->apply(x,y,res);
    }

    virtual void apply(domain_type& x, range_type& y, InverseOperatorResult& res){
      static_assert(Simd::lanes<typename solver_domain::field_type>() == 1 &&
                    Simd::lanes<typename solver_range::field_type>() == 1, "Solver for scalar valued vector expected");
      solver_domain solver_x(solver_.getInternalMatrix().M());
      solver_range solver_y(solver_.getInternalMatrix().N());
      for(size_t i = 0; i < Simd::lanes<field_type>(); ++i){
        for(size_t j = 0; j < solver_y.size(); ++j)
          solver_y[j] = Simd::lane(i, y[j]);
        solver_.apply(solver_x, solver_y, res);
        for(size_t j = 0; j < solver_x.size(); ++j)
          Simd::lane(i, x[j]) = solver_x[j];
      }
    }

    virtual void apply (domain_type& x, range_type& b, double reduction, InverseOperatorResult& res){
      DUNE_UNUSED_PARAMETER(reduction);
      apply(x,b,res);
    }

    template<class I>
    void setSubMatrix(const typename S::Matrix& _mat, const I& rowIndexSet)
    {
      solver_.setSubMatrix(_mat, rowIndexSet);
    }

    auto& getInternalMatrix()
    {
      return solver_.getInternalMatrix();
    }

    void decompose(){
      solver_.decompose();
    }

    const char* name() { return "VectorizedSubdomainSolver<"+solver_.name()+">"; }

  };

  template<class S, class X, class Y>
  struct IsDirectSolver<VectorizedSubdomainSolver<S,X,Y>>
  {
    enum { value=IsDirectSolver<S>::value };
  };

  template<class S, class X, class Y>
  struct StoresColumnCompressed<VectorizedSubdomainSolver<S,X,Y>>
  {
    enum { value = StoresColumnCompressed<S>::value };
  };
}
#endif
